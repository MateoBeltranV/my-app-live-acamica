# Acamica Sprint Project 2 (API persistente)

El presente es el proyecto de Acamica referente al sprint 2

## Instalación y tecnologías usadas

### Tecnologías usadas
| TECNOLOGIAS | ---- |
| ---- | ---- |
| Javascript |  nodeJS |
| Express |  Postman | 
| Helmet |  nodemon|
| Swagger|  Mocha |
| MySQL  |  Sequelize|
| Chai |  Chai-http |
| Javascript |  Dotenv |
| Jsonwebtoken |  Redis |
| Bcrypt | HeidiSQL |
 


### Instalación

Descargar el proyecto del repositorio remoto (anexo en el archivo de texto) e instalar los package del proyecto como se muestra a continuación

* Usar el git Bash o desde la terminal de comandos del Visual Studio Code para ejecutar 
```bash
npm install
```

## Ejecución
* Ejecutar el proyecto con el siguiente comando
```bash
npm run dev  o  nodemon src index.js
```
* Para verificar y ejecutar la rutina de test para registro use el siguiente comando
```bash
npm test
```

1. Al momento de ejecutar el proyecto (npm run dev), el código automáticamente generara las tablas (users, products, addresses, operations, orders y payMethods con sus respectivas asociaciones y ForeingKeys) dentro de la base de datos, con dos usuarios por defecto en la tabla users, uno ADMIN y el otro usuario.
2. Una vez desplegada las bases de datos, es importante generar la data para cada tabla, para esto, en la carpeta src/seeds existe un script formato SQL (databyDefault.sql) , ingrese a su términal MySQL (mysql> source /ubicacion del archivo SQL).
```bash
mysql -u root -p sprint2 source /ruta del archivo databyDefault.sql
```
```bash 
Enter password: ***    este password se encuentra en el archivo dotenv (DB_PASSWORD)
```

Nota: El script solo debe ser ejecutado una vez, para no duplicar datos en la base de datos. Si no sabe como realizar la ejecucion de este formato con MySQL, dirijase al siguiente tutorial url: [instructivo](https://www.youtube.com/watch?v=SGSzBqz30Rs)

3. Ingresar a la documentación de Swagger encontrada en la siguiente url: [documentation](http://localhost:3000/api-docs/)


## Uso
La API Delilah Restó, cumple con todos los requisitos exigidos por el cliente, tales como:
* Caché, testing y seguridad.
* El admin puede modificar el estado de las ordenes de los usuarios registrados (preparacion, enviada y entregada). Por supuesto los usuarios pueden ver en que estado está su pedido
* Todos los usuarios incluyendo el admin deben hacer un login para generar su respectivo TOKEN de seguridad y acceder a los servicios de la API
* Los usuarios pueden realizar pédidos todo lo que implica su respectivo CRUD, adicional, puede seleccionar **medios de pago y elegir una direccion desde su agenda de direcciones personalizable.**

Esta API ya tiene por defecto dos usuarios uno de ellos es el usuario administrador y el otro un usuario ordinario.

**Nota**: para efecto de sencillez el admin y la base de datos tienen el mismo password, el cual se encuentra en el dotenv  (DB_PASSWORD)

|Categoría | Usuario-correo | Contraseña |
| :---         |     :---:      |          ---: |
| Administrador  | admin@gmail.com     | ***   |
| Usuario    |jorge123@gmail.com  | jorge    |

Con estas instrucciones puede ejecutar los diferentes EndPoints declarados en la API.
