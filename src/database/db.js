require('dotenv').config();
const { Sequelize, DataTypes } = require('sequelize');


const { DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE } = process.env;

const sequelize = new Sequelize(DB_DATABASE, DB_USERNAME, DB_PASSWORD, {
    host: DB_HOST,
    dialect:  'mysql',
    // logging: false 
  });

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.Users = require('../models/usuario.model')(sequelize, DataTypes);
db.Products = require('../models/producto.model')(sequelize, DataTypes);
db.Orders = require('../models/ordenes.model')(sequelize, DataTypes);
db.PayMethods = require('../models/medioDePago')(sequelize, DataTypes);
db.Addresses = require('../models/direcciones.model')(sequelize, DataTypes);
db.Operations = require('../models/operaciones.model')(sequelize, DataTypes);

//bd for users
db.Users.hasMany(db.Orders);
db.Orders.belongsTo(db.Users);

db.Users.hasMany(db.Addresses);
db.Addresses.belongsTo(db.Users);

//BD for orders
db.Addresses.hasMany(db.Orders);
db.Orders.belongsTo(db.Addresses);

db.PayMethods.hasOne(db.Orders);
db.Orders.belongsTo(db.PayMethods);

db.Orders.hasMany(db.Operations);
db.Operations.belongsTo(db.Orders);

db.Products.hasMany(db.Operations);
db.Operations.belongsTo(db.Products);

module.exports = db;