const swaggerOptions = {
    definition:{
        openapi: "3.0.0",
        info: {
            title: "Sprint project 3 - Protalento",
            version: "3.0.0",
            description: "Proyecto 3 para acamica DWBE"
        },
        servers: [
            {
                url: "http://localhost:3000",
                description: "Local server"
            }
        ],
        components: {
            securitySchemes: {
                bearerAuth: {
                    type: "http",
                    scheme: "bearer",
                    bearerFormat: "JWT"
                }
            }
        },
        security: [
            {
                bearerAuth: []
            }
        ]

    },
    apis: ["./src/routes/*.js"]
};

module.exports = swaggerOptions;