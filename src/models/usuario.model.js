module.exports = ( sequelize, DataTypes ) =>{
    const User = sequelize.define('users', {
        name:{
            type: DataTypes.STRING,
            allowNull: false
        },
        password:{
            type: DataTypes.STRING,
            allowNull: false
        },
        email:{
            type: DataTypes.STRING, 
            allowNull: false
        },
        phone:{
            type: DataTypes.BIGINT,
            allowNull: false
        },
        isAdmin:{
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        isActive: {
            type: DataTypes.BOOLEAN,
            defaultValue: true,
        }
    }, {
        timestamps: false   
    });
    return User
};




// const usuarios = [
//     {   
//         email: "admin@gmail.com",
//         userName: "admin",
//         password: "12345",
//         userPhone: 3218304257,
//         userAdress: "villa claudio mz d #21",
//         id: 1,
//         isAdmin: true
//     }, 
//     {
//         email: "Mateo@gmail.com",
//         userName: "Mateo",
//         password: "12345t",
//         userPhone: 3113518004,
//         userAdress: "villa dora mz k #5",
//         id: 2,
//         isAdmin: false
//     },
//     {
//         email: "Ana@gmail.com",
//         userName: "Ana",
//         password: "12345",
//         userPhone: 3113558004,
//         userAdress: "pueblo Paleta mz k #5",
//         id: 3,
//         isAdmin: false
//     }
// ];

// //para retornar la lista de usuarios
// const obtenerUsuarios = () => {
//     return usuarios; 
// }

// const agregarUsuario = (usuarioNuevo) => {
//     usuarios.push(usuarioNuevo);
// }

// const encontrarUsuario = (userName, password) => {
//     let user = [];
//    user = obtenerUsuarios().find(u => u.userName === userName && u.password === password)
//    return user
// }

// module.exports = { encontrarUsuario, obtenerUsuarios, agregarUsuario}

// // usuario para probar en el body 
// // {
// //     "email": "real@gmail.com",
// //     "userName": "pikachu",
// //     "password": "12345",
// //     "userPhone": 3113558004,
// //     "userAdress": "pueblo Paleta mz k #5"
// // }