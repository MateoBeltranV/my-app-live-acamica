module.exports = (sequelize, DataTypes) => {
    const PayMethods = sequelize.define('payMethods', {
        payMethodName:{
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {
        timestamps: false   
    });
    return PayMethods
};


// const mediosDePago = [
//     { 
//         payName: "Tarjeta Debito Visa",
//         id: 1
//     },
//     { 
//         payName: "Ahorros Bancolombia",
//         id: 2
//     },
//     { 
//         payName: "Efectivo",
//         id: 3
//     },
//     { 
//         payName: "Cheque Bancario",
//         id: 4
//     },
//     { 
//         payName: "Cuenta corriente",
//         id: 5
//     },
//     { 
//         payName: "Cuenta de ahorros",
//         id: 6
//     },
//     { 
//         payName: "PSE",
//         id: 7
//     }
// ];


// const agregarMedioPago = (payName) => {
//     const macth = mediosDePago[mediosDePago.length-1].id+1;
//     const newProduct =    {   
//         id: macth,
//         payName: payName
//     }
//     mediosDePago.push(newProduct);
// }

// const obtenerMediosPago = () => {
//     return mediosDePago;
// }

// const eliminarMedioPago = (indexMedio) => {
//     mediosDePago.splice(indexMedio, 1);
// }

// module.exports = { agregarMedioPago, obtenerMediosPago, eliminarMedioPago };