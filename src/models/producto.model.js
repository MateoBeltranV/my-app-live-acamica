module.exports = ( sequelize, DataTypes ) => {
    const Product = sequelize.define('products', {
        productName:{
            type: DataTypes.STRING,
            allowNull: false
        },
        price:{
            type: DataTypes.FLOAT,
            allowNull: false
        }
    }, {
        timestamps: false   
    });
    return Product
};





// const productos = [
// {
//     id: 1,
//     productName: "Bagel de salmon",
//     precio: 425,
//     cantidad: 1
// },
// {
//     id: 2,
//     productName: "Hamburguesa clasica",
//     precio: 350,
//     cantidad: 1  
// },  
// {
//     id: 3,
//     productName: "Sandwich veggie",
//     precio: 310,
//     cantidad: 1   
// },  
// {
//     id: 4,
//     productName: "Ensalada veggie",
//     precio: 425,
//     cantidad: 1   
// },  
// {
//     id: 5,
//     productName: "Focaccia",
//     precio: 300,
//     cantidad: 1   
// },  
// {
//     id: 6,
//     productName: "Sandwich Focaccia",
//     precio: 440,
//     cantidad: 1   
// },  
// {
//     id: 7,
//     productName: "Agua",
//     precio: 100,
//     cantidad: 1  
// },
// {
//     id: 8,
//     productName: "Coca-Cola",
//     precio: 150,
//     cantidad: 1  
// }
// ];

// const obtenerProductos = () => {
//     return productos;
// }

// const agregarProductos = (productName, precio) => {
//     const macth = productos[productos.length-1].id+1;
//     const newProduct =    {   
//         id: macth,
//         productName: productName,
//         precio: precio,
//         q:1
//     }
//    productos.push(newProduct);
// }

// const eliminarProductos = (indexProducto) => {
//     productos.splice(indexProducto, 1);
// }


// module.exports = { agregarProductos, obtenerProductos, eliminarProductos };