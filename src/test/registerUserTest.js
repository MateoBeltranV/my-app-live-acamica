const chai = require('chai');
const chaiHttp = require('chai-http');
const db = require('../database/db');

//const userRouter = require('../routes/users.routes');
const app = require('../index');

chai.should();
chai.use(chaiHttp);



describe('POST /Registrar usuario exitosamente', () => {
    
    function registerClass(name, password, email, phone) {
        const User = { 
            name,
            password,
            email, 
            phone,
        };
        return User 
    }
   

    describe('/validacion de que el usuario este registrado', ()=>{
        
        it('Return status 200', (done) => {
            chai.request(app)
                .post('/usuarios/registrar')
                .send(registerClass('test', 'test123', 'test@gmail.com', 3145569))
                .end((err, response) => {
                    response.should.have.status(200);
                    response.should.be.an('object');
                    done();
                });
        });
    });
    
    after (async () =>{
        await db.Users.destroy({ 
            where: { 
                email: 'test@gmail.com' 
            }
        });
    });

});

