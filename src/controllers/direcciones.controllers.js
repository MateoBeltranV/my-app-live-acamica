const db = require("../database/db");

exports.getAll = async (req, res) => {
  const token = req.user.email;
  const userAdressInfo = await db.Users.findOne({
    where: {
      email: token,
    },
  });
  try {
    if (userAdressInfo) {
      const getAdress = await db.Addresses.findAll({
        where: {
          userId: userAdressInfo.id,
        },
      });
      res.status(200).json(getAdress);
    } else {
      res
        .status(400)
        .json(
          "Su token no coincide con la informacion requerida, verifique que su id de usuario exista"
        );
    }
  } catch (error) {
    res.status(401).json(error);
  }
};

exports.create = async (req, res) => {
  const { place, email } = req.body;
  const getUserEmail = await db.Users.findOne({
    where: {
      email,
    },
  });
  try {
    const newPlace = await db.Addresses.create({
      place,
      userId: getUserEmail.id,
    });
    res.status(200).json(newPlace);
  } catch (error) {
    res.status(401).json(error);
  }
};

exports.update = async (req, res) => {
  const { id } = req.params;
  const { place, email } = req.body;
  const getUserEmail = await db.Users.findOne({
    where: {
      email,
    },
  });
  const IdAdd = await db.Addresses.findOne({
    where: {
      id,
      userId: getUserEmail.id,
    },
  });

  try {
    if (IdAdd) {
      await db.Addresses.update(
        {
          place,
          userId: getUserEmail.id,
        },
        {
          where: {
            id,
          },
        }
      );
      res.status(201).json(`Direccion con id: ${id} actualizada`);
    } else {
      res
        .status(400)
        .json("El id del lugar solicitado no existe, por favor ingrese otro");
    }
  } catch (error) {
    res.status(401).json(error);
  }
};

exports.destroy = async (req, res) => {
  const { id } = req.params;
  const adressId = await db.Addresses.findOne({
    where:{
      id,
    },
  });
  try {
    if (adressId){
      await db.Addresses.destroy({
        where: {
          id
        }
      });
      res.status(200).json('Direccion eliminada');
    } else {
        res.status(400).json('El id ingresado del lugar no existe')
    }
  } catch (error) {
      res.status(401).json(error); 
  }
};
