const db = require("../database/db");
const bcrypt = require("bcrypt");
const jsonwebtoken = require("jsonwebtoken");
const loginValidation = require("../schemas/loginSchema");
const userValidation = require("../schemas/usuarioSchema");
require('dotenv').config('../.env');
const jwtPassword = process.env.SERVER_PASSWORD;

exports.getAll = async (req, res) => {
  try {
  const getUsers = await db.Users.findAll();
    if(getUsers) {
      res.json(getUsers);
    }
  } catch (error) {
    res.status(404).json(error);
  }
};

exports.registro = async (req, res) => {
  try {
    const { name, password, email, phone} = await userValidation.validateAsync(
      req.body
    );
    const newUser = await db.Users.create({
      name,
      password: bcrypt.hashSync(password, 10),
      email,
      phone
    });
    res.status(200).json(newUser);
  } catch (error) {
    res.status(404).json(error);
  }
};

exports.login = async (req, res) => {
  try {
    //con validateAsync validamos un body usando el esquema y las opciones, luego usamos el destructuring para obtener email y password
    const { email, password } = await loginValidation.validateAsync(req.body);
    //password a userPassword
    const { isAdmin, password: userPassword } = await db.Users.findOne({
      where: {
        email,
      },
    });
    const responseLogin = bcrypt.compareSync(password, userPassword);
    if (responseLogin) {
      const token = jsonwebtoken.sign(
        {
          email,
          isAdmin,
        },
        jwtPassword
      );
      res.json("token: " + token);
    } else {
      res.status(404).json("Unathorized desde userio.controllers");
    }
  } catch (error) {
    res.status(404).json(error);
  }
};

exports.updateState = async (req, res) => {
  const { id } = req.params;
  const { isActive } = req.body;
  const findId = await db.Users.findOne({
    where: {
      id,
    },
  });
  if (findId) {
    const updateStateUser = await db.Users.update(
      { isActive },
      {
        where: {
          id,
        },
      }
    );
    res
      .status(200)
      .json(
        `Usuario ${findId.name} actualizado, ahora su isActive esta: ${isActive}`
      );
  } else {
    res
      .status(404)
      .json(
        "Porfavor verifique el el id del usuario a actualizar existe en nuestra BD"
      );
  }
};
