const db = require('../database/db');

exports.getAll = async(req, res) => {
    try {
        const getAllPayMeth = await db.PayMethods.findAll();
        res.status(200).json(getAllPayMeth);   
    } catch (error) {
        res.status(400).json(error);
    }
};

exports.create = async(req, res) => {
    const { payMethodName } = req.body;
    const findMethodName = await db.PayMethods.findOne({
        where: {
            payMethodName,
        },
    });
    try {
        if( !findMethodName ) {
            const newPayMethod = await db.PayMethods.findOrCreate({
                where:{
                    payMethodName,
                },
            });
            res.status(200).json('se ha creado con exito el nuevo metodo de pago ' + payMethodName);
        } else res.status(400).json('Este metodo de pago ya existe');
    } catch (error) {
        res.status(400).json(error)
    }
};

exports.update = async(req, res) => {
    const { id } = req.params;
    const { payMethodName } = req.body
    const findMethodName = await db.PayMethods.findOne({
        where:{
            id
        }
    });
    try {
        if( findMethodName ){
         await db.PayMethods.update({ payMethodName },{
                where:{
                    id
                }
            });
            res.status(200).json(`Medio de pago con id: ${id} actualizado correctamente a ${payMethodName}`);
        }else{
            res.status(400).json('El id del metodo de pago no existe, porfavor verifique su solicitud');
        }
    } catch (error) {
        res.status(400).json(error);
    }
};

exports.destroy = async(req, res) => {
    const { id } = req.params;
    const findMethodName = await db.PayMethods.findOne({
        where: {
            id
        }
    });
    try {
        if( findMethodName ){
            await db.PayMethods.destroy({
                where: {
                    id
                }
            });
            res.status(200).json('se ha eliminado el metodo ' + findMethodName.payMethodName + ' exitosamente')
        }
        else{
            res.status(400).json('El id del metodo de pago no existe, por favor verifique su solicitud');
        } 
    } catch (error) {
        res.status(400).json(error);
    }
};