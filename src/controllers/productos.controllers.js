const db = require("../database/db");
const redis = require('redis');
//puerto por default en redis
const clientRedis = redis.createClient(6379);

exports.getAll = async (req, res) => {
  try {
      const getProducts = await db.Products.findAll();
      clientRedis.setex('productos', 60 * 2, JSON.stringify(getProducts));
      res.status(200).json(getProducts);
  } catch (error) {
      res.status(400).json(error);
  }
};

exports.create = async (req, res) => {
  const { productName, price } = req.body;
  try {
    await db.Products.findOrCreate({
      where: {
        productName: req.body.productName,
        price: req.body.price,
      },
      defaults: {
        productName: req.body.productName,
        price: req.body.price,
      },
    });
    clientRedis.del('productos');
    res.status(200).json(`Producto ${productName} creado`);
  } catch (error) {
    res.status(400).json(error);
  }
};

exports.update = async (req, res) => {
  const { id } = req.params;
  const { productName, price } = req.body;

  const findIdProduct = await db.Products.findOne({
    where: {
      id,
    },
  });
  try {
    if (findIdProduct) {
      await db.Products.update(
        {
          productName,
          price,
        },
        {
          where: {
            id,
          },
        }
      );
      clientRedis.del('productos');
      res.json(`Producto con id ${id} actualizado`);
    } else {
      res
        .status(400)
        .json(
          "El id del producto a actualizar no existe, por favor verifique su solicitud"
        );
    }
  } catch (error) {
    res.status(400).json(error);
  }
};

exports.destroy = async (req, res) => {
  const { id } = req.params;
  const findIdProduct = await db.Products.findOne({
    where: {
      id,
    },
  });

  try {
    if (findIdProduct) {
      await db.Products.destroy({ where: { id } });
      res.status(200).json(`producto ${ findIdProduct.productName } eliminado`);
      clientRedis.del('productos');
    } else {
      res
        .status(400)
        .json(
          "El id del producto a eliminar no existe porfavor, verifique su solicitud"
        );
}
  } catch (error) {
    res.status(400).json(error);
  }
};
