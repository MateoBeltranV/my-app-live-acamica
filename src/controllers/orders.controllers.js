const db = require("../database/db");
const { Op } = require("sequelize");

exports.getAll = async (req, res) => {
  try {
    const getAllOrders = await db.Users.findAll({
      include: ["orders", "addresses"],
    });
    res.json(getAllOrders);
  } catch (error) {
    res.status(400).json(error);
  }
};

//Obtener orden del usuario con id user
exports.getOrderUser = async (req, res) => {
  const emailToken = req.user.email;
  const userId = await db.Users.findOne({
    where: {
      email: emailToken,
    },
  });
  try {
    const userOrders = await db.Orders.findAll({
      where: {
        userId: userId.id,
        stateOrder: "pendiente",
      },
      include: ["operations"],
    });
    res.status(200).json(userOrders);
  } catch (error) {
    res.status(400).json(error);
  }
};

//anadir un producto a una orden, o crearlo si no existe
exports.create = async (req, res) => {
  const { id } = req.params;
  const { email } = req.body;
  const getProduct = await db.Products.findOne({ where: { id } });
  const getUser = await db.Users.findOne({ where: { email: email } });
  const getOrder = await db.Orders.findOne({
    where: { userId: getUser.id, stateOrder: "pendiente" },
  });
  const getOrderComplete = await db.Orders.findOne({
    where: { userId: getUser.id, stateOrder: !"pendiente" },
  });

  try {
    if (getProduct) {
      if (!getOrder || getOrder.stateOrder != "pendiente") {
        const newOrder = await db.Orders.create({ userId: getUser.id });
        const operation = await db.Operations.create({
          productId: getProduct.id,
          NameProduct: getProduct.productName,
          ValueProduct: getProduct.price,
          orderId: newOrder.id,
        });
        //agrega atributos de la tabla Operations a la nueva orden
        await newOrder.addOperations(operation, {
          through: { selfGranted: false },
        });
        const Result = await db.Orders.findOne({
          where: {
            userId: getUser.id,
            stateOrder: "pendiente",
          },
          include: ["operations"],
        });
        const oper = await db.Operations.findAll({
          where: {
            orderId: Result.id,
          },
        });
        const howPay = await oper.reduce(
          (a, b) => a + b.ValueProduct * b.quantity,
          0
        );
        await db.Orders.update(
          {
            totalCost: howPay,
          },
          {
            where: {
              id: Result.id,
            },
          }
        );

        res.status(201).json(Result.operations);
      } else {
        const getOperation = await db.Operations.findOne({
          where: {
            orderId: getOrder.id,
            productId: getProduct.id,
          },
        });

        if (getOperation) {
          const { quantity } = await db.Operations.findOne({
            where: {
              orderId: getOrder.id,
              productId: getProduct.id,
            },
          });
          await db.Operations.update(
            {
              quantity: quantity + 1,
            },
            {
              where: {
                orderId: getOrder.id,
                productId: getProduct.id,
              },
            }
          );
          const Result = await db.Orders.findOne({
            where: {
              userId: getUser.id,
              stateOrder: "pendiente",
            },
            include: ["operations"],
          });
          const oper = await db.Operations.findAll({
            where: {
              orderId: Result.id,
            },
          });
          const howPay = await oper.reduce(
            (a, b) => a + b.ValueProduct * b.quantity,
            0
          );
          await db.Orders.update(
            {
              totalCost: howPay,
            },
            {
              where: {
                id: Result.id,
              },
            }
          );
          res.status(200).json(Result.operations);
        } else {
          const Order = await db.Orders.findOne({
            where: {
              userId: getUser.id,
              stateOrder: "pendiente",
            },
          });
          const operation = await db.Operations.create({
            productId: getProduct.id,
            NameProduct: getProduct.productName,
            ValueProduct: getProduct.price,
            orderId: Order.id,
          });

          await Order.addOperations(operation, {
            through: { selfGranted: false },
          });

          const Result = await db.Orders.findOne({
            where: {
              userId: getUser.id,
              stateOrder: "pendiente",
            },
            include: ["operations"],
          });

          const oper = await db.Operations.findAll({
            where: {
              orderId: Result.id,
            },
          });

          const howPay = await oper.reduce(
            (a, b) => a + b.ValueProduct * b.quantity,
            0
          );

          await db.Orders.update(
            {
              totalCost: howPay,
            },
            {
              where: {
                id: Result.id,
              },
            }
          );
          res.status(200).json(Result.operations);
        }
      }
    } else {
      res.status(400).json("El id del producto ingresado no existe");
    }
  } catch (error) {
    res.status(500).json(error);
  }
};

exports.deleteProduct = async (req, res) => {
  const { id } = req.params;
  const { email } = req.body;

  const getProduct = await db.Products.findOne({ where: { id } });
  const getUser = await db.Users.findOne({ where: { email: email } });
  const getOrder = await db.Orders.findOne({
    where: { userId: getUser.id, stateOrder: "pendiente" },
  });

  if (getProduct) {
    if (!getOrder) {
      res
        .status(400)
        .json(
          "Actualmente no tiene ordenes pendientes, por ende no se puede realizar su peticion"
        );
    } else {
      const getOperation = await db.Operations.findOne({
        where: {
          orderId: getOrder.id,
          productId: getProduct.id,
        },
      });

      if (getOperation) {
        if (getOperation.quantity > 1) {
          await db.Operations.update(
            {
              quantity: getOperation.quantity - 1,
            },
            {
              where: {
                orderId: getOrder.id,
                productId: getProduct.id,
              },
            }
          );

          const oper = await db.Operations.findAll({
            where: {
              orderId: getOrder.id,
            },
          });

          const howPay = await oper.reduce(
            (a, b) => a + b.ValueProduct * b.quantity,
            0
          );
          await db.Orders.update(
            {
              totalCost: howPay,
            },
            {
              where: {
                id: getOrder.id,
                stateOrder: "pendiente",
              },
            }
          );

          res
            .status(200)
            .json(
              `se ha elimanado una  ${getProduct.productName} del total del pedido`
            );
        } else {
          await db.Operations.destroy({
            where: {
              productId: getProduct.id,
            },
          });

          const oper = await db.Operations.findAll({
            where: {
              orderId: getOrder.id,
            },
          });

          const howPay = await oper.reduce(
            (a, b) => a + b.ValueProduct * b.quantity,
            0
          );
          await db.Orders.update(
            {
              totalCost: howPay,
            },
            {
              where: {
                id: getOrder.id,
                stateOrder: "pendiente",
              },
            }
          );

          res
            .status(200)
            .json(`producto  ${getProduct.productName} eliminado de la orden`);
        }
      } else {
        res.status(400).json("El id del producto en la orden no existe");
      }
    }
  } else {
    res.status(400).json("El id del producto a eliminar no existe");
  }
};

// añadidos a la orden dirección/metodo de pago/confirmar orden/cambiar el estado de una orden por el admin
//Obtener orden de usuario por id 
exports.adminGetUserOrder = async (req, res) => {
    const { id } = req.params;
    try {
        const getAllOrders = await db.Users.findOne({
            include: ['orders', 'addresses'],
            where:{
                id
            }
        });
        res.status(200).json(getAllOrders);
    } catch (error) {
        res.status(400).json(error);
    }
};

// el usuario elige el metodo de pago
exports.PayMeth = async (req, res) =>{
    const { id } = req.params;
    const { email } =  req.body;

    const getPayMeth = await db.PayMethods.findOne({ where: { id }  });
    const getUser = await db.Users.findOne({ where: { email } });
    const getOrder = await db.Orders.findOne({ where: { userId: getUser.id, stateOrder: 'pendiente' } });
//añade otra propiedad a la orden
    if( getPayMeth ){
        if( getOrder ){
            await db.Orders.update({ 
                payMethodId: getPayMeth.id
             },{ 
                where:{ 
                    id: getOrder.id   
                  }
            });

            res.status(200).json(`Usted a seleccionado ${getPayMeth.payMethodName} como metodo de pago`);
        }else{
            res.status(400).json('Usted no tiene ordenes en proceso');
        }


    }else{
        res.status(400).json('El id del metodo de pago no existe');    
    }
};

// el usuario elige la dirección
exports.selectAdress = async (req, res) => {
    const { id } = req.params;
    const { email } =  req.body;

    const getUser = await db.Users.findOne({ where: { email } });
    const getAdress = await db.Addresses.findOne({ where: { id, userId: getUser.id } });
    const getOrder = await db.Orders.findOne({ where: { userId: getUser.id, stateOrder: 'pendiente' } });

    if (getAdress) {
        if (getOrder) {
            await db.Orders.update({
                addressId: getAdress.id
            }, {
                where:{
                    id: getOrder.id
                }
            })
            res.status(200).json('Se ha tomado con éxito ' + getAdress.place + ' como dirección de envio');

        } else {
            res.status(400).json('Usted no cuenta con un pedido aún');
        }
    } else {
        res.status(400).json('El id de la direccion no existe en su lista');
    }
};

//El usuario confirma la orden
exports.confirmOrder = async (req, res) => {
  const { id } = req.params;
  const { email } = req.body;

  const getUser = await db.Users.findOne({ where: { email } });
  const getOrder = await db.Orders.findOne({ where: { id, userId: getUser.id, stateOrder: 'pendiente' }});
  
  if(getOrder){
    if((getOrder.addressId, getOrder.payMethodId) == null){
      res.status(400).json('Por favor antes de confirmar su orden proporcione una direccion y un metodo de pago')
    }
    else{
      const ordConfirm = await db.Orders.update(
        {
        stateOrder: 'confirmada'
        }, {
        where: {
          id: getOrder.id,
          userId: getUser.id,
          stateOrder: 'pendiente'
        }
      });
      res.status(200).json('Su orden ha cambiado de estado pendiente a confirmada');
    }
  } 
  else {
    res.status(400).json('Estimado usuario usted no tiene ordenes pendientes o el id de la Orden no existe en su lista');
  }
};

//El admin cambia el estado de la orden
exports.changeStateOrder = async (req, res) => {
  const { id } = req.params;
  const { stateOrder } = req.body;
  const Opcion = 'confirmada';
  const Opcion1 = 'preparacion';
  const Opcion2 = 'enviada';
  const Opcion3 = 'entregada';
  const getOrderComprabation = await db.Orders.findOne({ where: { id, stateOrder: { [Op.ne]: 'pendiente' } }});
  

  if ( Opcion1 == stateOrder || Opcion2 == stateOrder || Opcion3 == stateOrder ) {
      if( getOrderComprabation ){
          await db.Orders.update({
              stateOrder
          }, {
              where: {
                  id
              }
          });
  
          res.status(201).json('La orden con id: ' + id + ' a sido actualizada a estado de: ' + stateOrder );
  
      }else{
          res.status(400).json('El id de la orden no se encuentra en estado de ' + Opcion + ' no existe');
      }
  } else {
      res.status(400).json('Por favor ingrese un estado de orden valido');
  }
};