const express = require('express');
const router = express.Router();

const PayMethod = require('../controllers/payMethods.controllers');
const middlewareU = require('../middlewares/usersMiddlewares');

router.use('/', middlewareU.expJWT,  middlewareU.invalidToken, middlewareU.uStateActive);
// router.use('/getPayMethod', middlewareU.expJWT, middlewareU.invalidToken );
router.use('/create', middlewareU.AdminToken, middlewareU.emailToken);
router.use('/update', middlewareU.AdminToken, middlewareU.emailToken);
router.use('/delete', middlewareU.AdminToken, middlewareU.emailToken);

/**
 * @swagger
 * /medioDePago/getPayMethods:
 *  get:
 *      summary: Obtener todos medios de pago del sistema
 *      tags: [Métodos de pago]
 *      responses:
 *          200:
 *              description: Lista de medios de pago en el sistema
 *          400:
 *              description: error catch                                                        
 */
router.get('/getPayMethods', PayMethod.getAll);

/**
 * @swagger
 * /medioDePago/create:
 *  post:
 *      summary: ADMIN Crea un nuevo metodo de pago en el sistema
 *      tags: [Métodos de pago]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/postPayMeth'
 *      responses:
 *          200:
 *              description: medio de pago creado.
 *          400: 
 *              description: error catch.
 *          403:
 *              description: usted no cuenta con permisos ADMIN.
 */
router.post('/create', PayMethod.create);

/**
 * @swagger
 * /medioDePago/update/{id}:
 *  put:     
 *      parameters:
 *          -  in: path
 *             name: id
 *             description: id del metodo de pago a actualizar
 *             required: true
 *             type: integer
 * 
 *      summary: ADMIN accion para actualizar un medio de pago
 *      tags: [Métodos de pago]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/updatePayMeth'
 *      responses:
 *          200:
 *              description: El medio de pago a sido modificado
 *          400:
 *              description: El id del medio de pago a actualizar no existe, por favor verifique su solicitud
 *      
 */
router.put('/update/:id', PayMethod.update);

/**
 * @swagger
 * /medioDePago/delete/{id}:
 *  delete:     
 *      parameters:
 *          -  in: path
 *             name: id
 *             description: id del metodo de pago a actualizar
 *             required: true
 *             type: integer
 * 
 *      summary: ADMIN accion para eliminar un medio de pago
 *      tags: [Métodos de pago]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/delPayMeth'
 *      responses:
 *          200:
 *              description: El medio de pago sido eliminado
 *          400:
 *              description: El id del medio de pego a eliminar no existe, por favor verifique su solicitud
 *      
 */
router.delete('/delete/:id', PayMethod.destroy);

/**
 * @swagger 
 * tags: 
 *  name : 'Métodos de pago'
 *  description: 'CRUD para metodos de pago en Delilah Resto web'
 * 
 * components: 
 *  schemas:
 *      postPayMeth: 
 *          type: object
 *          required:
 *               -email
 *               -payMethodName
 *          properties:
 *              email:
 *                  type: string
 *                  example: admin@gmail.com
 *                  description: email del ADMIN
 *              payMethodName: 
 *                  type: string
 *                  example: Nequi
 *                  description: nombre del metodo de pago
 *                  $ref: '#/components/schemas/postPayMeth'               
 */

/**
 * @swagger 
 * tags: 
 *  name : 'Métodos de pago'
 *  description: 'CRUD para metodos de pago en Delilah Resto web'
 * 
 * components: 
 *  schemas:
 *      delPayMeth: 
 *          type: object
 *          required:
 *               -email
 *          properties:
 *              email:
 *                  type: string
 *                  example: admin@gmail.com
 *                  description: email del ADMIN
 *                  $ref: '#/components/schemas/delPayMeth'               
 */

/**
 * @swagger 
 * tags: 
 *  name : 'Métodos de pago'
 *  description: 'CRUD para metodos de pago en Delilah Resto web'
 * 
 * components: 
 *  schemas:
 *      updatePayMeth: 
 *          type: object
 *          required:
 *               -email
 *               -payMethodName
 *          properties:
 *              email:
 *                  type: string
 *                  example: admin@gmail.com
 *                  description: email del ADMIN
 *              payMethodName: 
 *                  type: string
 *                  example: Daviplata
 *                  description: nombre del metodo de pago
 *                  $ref: '#/components/schemas/updatePayMeth'               
 */

module.exports = router;