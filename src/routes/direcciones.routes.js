const express = require('express');
const router = express.Router();

const Adress = require('../controllers/direcciones.controllers');
const middlewareU = require('../middlewares/usersMiddlewares');

router.use('/', middlewareU.expJWT,  middlewareU.invalidToken, middlewareU.uStateActive);

/**
 * @swagger
 * /direcciones/getUserAdress:
 *  get:
 *      summary: El usuario obtiene sus direcciones
 *      tags: [Agenda de direcciones]
 *      responses:
 *          200:
 *              description: Lista de direcciones de usuarios en el sistema
 *          400:
 *              description: error catch                                                        
 */
router.get('/getUserAdress',  Adress.getAll );

/**
 * @swagger
 * /direcciones/create:
 *  post:
 *      summary: Crea una nueva direccion en tu perfil
 *      tags: [Agenda de direcciones]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/postAddres'
 *      responses:
 *          200:
 *              description: direccion creada.
 *          400: 
 *              description: error catch.
 */
router.post('/create', middlewareU.emailToken, Adress.create);

/**
 * @swagger
 * /direcciones/update/{id}:
 *  put:     
 *      parameters:
 *          -  in: path
 *             name: id
 *             description: id de la direccion a actualizar
 *             required: true
 *             type: integer
 * 
 *      summary: accion para actualizar una direccion de tu perfil
 *      tags: [Agenda de direcciones]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/postAddres'
 *      responses:
 *          200:
 *              description: La direccion ha sido modificada
 *          400:
 *              description: El id de la direccion a actualizar no existe, por favor verifique su solicitud
 *      
 */
router.put('/update/:id', middlewareU.emailToken, Adress.update);


/**
 * @swagger
 * /direcciones/delete/{id}:
 *  delete:     
 *      parameters:
 *          -  in: path
 *             name: id
 *             description: id de la direccion a eliminar
 *             required: true
 *             type: integer
 * 
 *      summary: accion para eliminar una direccion de tu perfil
 *      tags: [Agenda de direcciones]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/delete'
 *      responses:
 *          200:
 *              description: El producto ha sido eliminado
 *          400:
 *              description: El id del producto a eliminar no existe, por favor verifique su solicitud
 *      
 */
router.delete('/delete/:id',  middlewareU.emailToken, Adress.destroy );

/**
 * @swagger 
 * tags: 
 *  name : 'Agenda de direcciones'
 *  description: 'CRUD para direcciones de usuarios en Delilah Resto web'
 * 
 * components: 
 *  schemas:
 *      delete: 
 *          type: object
 *          required:
 *               -email
 *          properties:
 *              email:
 *                  type: string
 *                  example: jorge123@gmail.com
 *                  description: email del usuario
 *                  $ref: '#/components/schemas/delete'
 */

/**
 * @swagger 
 * tags: 
 *  name : 'Agenda de direcciones'
 *  description: 'CRUD para direcciones de usuarios en Delilah Resto web'
 * 
 * components: 
 *  schemas:
 *      postAddres: 
 *          type: object
 *          required:
 *               -email
 *               -place
 *          properties:
 *              email:
 *                  type: string
 *                  example: jorge123@gmail.com
 *                  description: email de user
 *              place: 
 *                  type: string
 *                  example: Avenida Bolivar cr-24
 *                  description: direccion del user
 *                  $ref: '#/components/schemas/postAddres'                 
 */

module.exports = router;