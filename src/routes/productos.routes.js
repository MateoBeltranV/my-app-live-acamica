const express = require('express');
const router = express.Router();

const Product = require('../controllers/productos.controllers');
const middlewareU = require('../middlewares/usersMiddlewares')
const Cache = require('../middlewares/cache');

router.use('/', middlewareU.expJWT,  middlewareU.invalidToken, middlewareU.uStateActive);
router.use('/allProducts',  middlewareU.invalidToken);
router.use('/create', middlewareU.AdminToken, middlewareU.emailToken);
router.use('/update', middlewareU.AdminToken, middlewareU.emailToken);
router.use('/delete', middlewareU.AdminToken, middlewareU.emailToken);


/**
 * @swagger
 * /productos/allProducts:
 *  get:
 *      summary: Obtener todos productos de la app
 *      tags: [Productos]
 *      responses:
 *          200:
 *              description: Lista de productos en el sistema
 *          400:
 *              description: error catch                                                        
 */
router.get('/allProducts', Cache.CacheProducts, Product.getAll);

/**
 * @swagger
 * /productos/create:
 *  post:
 *      summary: ADMIN Crea un nuevo producto en el sistema
 *      tags: [Productos]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/postProducts'
 *      responses:
 *          200:
 *              description: producto creado.
 *          400: 
 *              description: error catch.
 *          403:
 *              description: usted no cuenta con permisos ADMIN.
 */
router.post('/create', Product.create );

/**
 * @swagger
 * /productos/update/{id}:
 *  put:     
 *      parameters:
 *          -  in: path
 *             name: id
 *             description: id del producto a actualizar
 *             required: true
 *             type: integer
 * 
 *      summary: ADMIN  para actualizar un producto
 *      tags: [Productos]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/updateProducts'
 *      responses:
 *          200:
 *              description: El producto ha sido modificado
 *          400:
 *              description: El id del producto a actualizar no existe, por favor verifique su solicitud
 *      
 */
router.put('/update/:id', Product.update);

/**
 * @swagger
 * /productos/delete/{id}:
 *  delete:     
 *      parameters:
 *          -  in: path
 *             name: id
 *             description: id del producto a dar de baja
 *             required: true
 *             type: integer
 * 
 *      summary: ADMIN accion para eliminar un producto
 *      tags: [Productos]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/allProducts'
 *      responses:
 *          200:
 *              description: El producto ha sido eliminado
 *          400:
 *              description: El id del producto a eliminar no existe, por favor verifique su solicitud
 *      
 */
router.delete('/delete/:id', Product.destroy)

/**
 * @swagger 
 * tags: 
 *  name : 'Productos'
 *  description: 'CRUD para productos en Delilah Resto web'
 * 
 * components: 
 *  schemas:
 *      allProducts: 
 *          type: object
 *          required:
 *               -email
 *          properties:
 *              email:
 *                  type: string
 *                  example: admin@gmail.com
 *                  description: email del ADMIN
 *                  $ref: '#/components/schemas/allProducts'
 */

/**
 * @swagger 
 * tags: 
 *  name : 'Productos'
 *  description: 'CRUD para productos en Delilah Resto web'
 * 
 * components: 
 *  schemas:
 *      postProducts: 
 *          type: object
 *          required:
 *               -email
 *               -productName
 *               -price
 *          properties:
 *              email:
 *                  type: string
 *                  example: admin@gmail.com
 *                  description: email del ADMIN
 *              productName: 
 *                  type: string
 *                  example: Soda
 *                  description: nombre producto 
 *              price: 
 *                  type: number
 *                  example: 3000
 *                  description: precio producto
 *                  $ref: '#/components/schemas/postProducts'                 
 */

/**
 * @swagger 
 * tags: 
 *  name : 'Productos'
 *  description: 'CRUD para productos en Delilah Resto web'
 * 
 * components: 
 *  schemas:
 *      updateProducts: 
 *          type: object
 *          required:
 *               -email
 *               -productName
 *               -price
 *          properties:
 *              email:
 *                  type: string
 *                  example: admin@gmail.com
 *                  description: email del ADMIN
 *              productName: 
 *                  type: string
 *                  example: Agua de panela con queso
 *                  description: nombre producto 
 *              price: 
 *                  type: number
 *                  example: 5000
 *                  description: precio producto
 *                  $ref: '#/components/schemas/updateProducts'                 
 */

module.exports = router;