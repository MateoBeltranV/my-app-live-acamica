const express = require('express');
const router = express.Router();
const User = require('../controllers/usuario.controllers');
const middlewareU = require('../middlewares/usersMiddlewares')

//validaciones para los usuarios

router.use('/allUsers', middlewareU.expJWT,  middlewareU.invalidToken, middlewareU.AdminToken);
router.use('/registrar', middlewareU.NoRepeatUsers)

//al realizar el request no olvidar agregar a la ruta /usuarios como la registrada en el index
/**
 * @swagger
 * /usuarios/allUsers:
 *  get:
 *      summary: ADMIN Obtener todos los usuarios resgitrados en tu sistema 
 *      tags: [Usuarios]
 *      responses:
 *          200:
 *              description: Lista de usuarios del sistema
 *                             
 *                              
 */
router.get('/allUsers', User.getAll );

/**
 * @swagger
 * /usuarios/registrar:
 *  post:
 *      summary: Crea un nuevo usuario en el sistema
 *      tags: [Usuarios]
 *      security: []
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/registro'
 *      responses:
 *          200:
 *              description: Usuario creado.
 *          400: 
 *              description: El email suministrado ya existe.
 *          404:
 *              description: error catch
 */
router.post('/registrar', User.registro);

/**
 * @swagger
 * /usuarios/login:
 *  post:
 *      summary: Ingresa con tu correo y contraseña sistema
 *      tags: [Usuarios]
 *      security: []
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/LogIn'
 *      responses:
 *          200:
 *              description: Todo en orden su token es
 *          404:
 *              description: No esta autorizado.
 */
router.post('/login', User.login);

/**
 * @swagger
 * /usuarios/updateUser/{id}:
 *  put:     
 *      parameters:
 *          -  in: path
 *             name: id
 *             description: id del usuario a actualizar
 *             required: true
 *             type: integer
 * 
 *      summary: El ADMIN puede suspender a un usuario
 *      tags: [Usuarios]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/UpdateUser'
 *      responses:
 *          200:
 *              description: El usuario ha sido modificado
 *          404:
 *              description: Por favor verifique que el id del usuario este disponible en la BD
 *      
 */
router.put('/updateUser/:id', User.updateState);

//schemas
//usuario creado 
/**
 * @swagger
 * tags:
 *  name: 'Usuarios'
 *  description: 
 * components:
 *  schemas:
 *      registro:
 *          type: object
 *          required:
 *              -name
 *              -password
 *              -email
 *              -phone
 *          properties:
 *              name:
 *                  type: string
 *                  example: Danilo Beltran
 *                  description: nombre unico usuario
 *              password: 
 *                  type: string
 *                  example: danilo1
 *                  description: password
 *              email:
 *                  type: string
 *                  example: danilo@gmail.com
 *                  description: email del usuario
 *              phone:
 *                  type: number
 *                  example: 7445445
 *                  description: numumero del usuario
 *                  $ref: '#/components/schemas/SingIn'
 */

/**
 * @swagger 
 * tags: 
 *  name : 'Usuarios'
 *  description: 'Login in Delilah Resto web'
 * 
 * components: 
 *  schemas:
 *      LogIn: 
 *          type: object
 *          required:
 *               -email
 *               -password
 *          properties:
 *              email:
 *                  type: string
 *                  example: admin@gmail.com
 *                  description: email del usuario
 *              password: 
 *                  type: string
 *                  example: 123
 *                  description: password
 *                  $ref: '#/components/schemas/LogIn'
 */

/**
 * @swagger 
 * tags: 
 *  name : 'Usuarios'
 *  description: 'LogIn in Delilah Resto web'
 * 
 * components: 
 *  schemas:
 *      UpdateUser: 
 *          type: object
 *          required:
 *               -isActive
 *          properties:
 *              isActive: 
 *                  type: boolean
 *                  example: false
 *                  description: modifica el estado de un usuario
 *                  $ref: '#/components/schemas/UpdateUser'
 */

module.exports = router;