const express = require('express');
const router = express.Router();

const Order = require('../controllers/orders.controllers');
const middlewareU = require('../middlewares/usersMiddlewares');

router.use('/', middlewareU.expJWT,  middlewareU.invalidToken, middlewareU.uStateActive);

/**
 * @swagger
 * /ordenes/getAllorders:
 *  get:
 *      summary: El admin obtiene todas las ordenes del sistema
 *      tags: [Ordenes]
 *      responses:
 *          200:
 *              description: Lista de ordenes en el sistema
 *          400:
 *              description: error catch                                                        
 */
router.get('/getAllorders', middlewareU.AdminToken, Order.getAll);

/**
 * @swagger
 * /ordenes/getUserOrder:
 *  get:
 *      summary: Observa como va tu orden en Dalilah Resto
 *      tags: [Ordenes]
 *      responses:
 *          200:
 *              description: Lista de productos en el sistema
 *          400:
 *              description: error catch                                                        
 */
router.get('/getUserOrder', Order.getOrderUser); //el usuario obtiene su orden

/**
 * @swagger
 * /ordenes/adminGetUserOrder/{id}:
 *  get:
 *      parameters:
 *          -  in: path
 *             name: id
 *             description: id del usuario del historial de sus ordenes
 *             required: true
 *             type: integer
 *      summary: El Admin obtiene las ordenes de un usuario del sistema
 *      tags: [Ordenes]
 *      responses:
 *          200:
 *              description: Lista de ordenes en el sistema del usuario seleccionado
 *          400:
 *              description: error catch                                                        
 */
 router.get('/adminGetUserOrder/:id', middlewareU.AdminToken, Order.adminGetUserOrder );  //obtener la orden de un usuario por id, de parte del admin para cambiar su estado

/**
 * @swagger
 * /ordenes/create/{id}:
 *  post:
 *      parameters:
 *          -  in: path
 *             name: id
 *             description: id del producto agregar a la orden
 *             required: true
 *             type: integer
 *      summary: Agrega un nuevo producto en tu order
 *      tags: [Ordenes]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/postOrders'
 *      responses:
 *          201:
 *              description: producto agregado en la orden.
 *          500: 
 *              description: error catch.
 *          400:
 *              description: el id del producto no existe en nuestra DB
 */
router.post('/create/:id', middlewareU.emailToken, Order.create);

/**
 * @swagger
 * /ordenes/delete/{id}:
 *  delete:     
 *      parameters:
 *          -  in: path
 *             name: id
 *             description: id del producto a eliminar de la orden
 *             required: true
 *             type: integer
 * 
 *      summary: Se elimina un producto de tu orden actual
 *      tags: [Ordenes]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/postOrders'
 *      responses:
 *          200:
 *              description: El producto ha sido eliminado de la orden
 *          400:
 *              description: El id del producto a eliminar no existe, por favor verifique su solicitud
 *      
 */
router.delete('/delete/:id', middlewareU.emailToken, Order.deleteProduct);

/**
 * @swagger
 * /ordenes/selectPayMeth/{id}:
 *  put:     
 *      parameters:
 *          -  in: path
 *             name: id
 *             description: id del metodo de pago para la orden
 *             required: true
 *             type: integer
 * 
 *      summary: accion para seleccionar el metodo de pago para la orden actual
 *      tags: [Ordenes]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/postOrders'
 *      responses:
 *          200:
 *              description: El metodo de pago ha sido seleccionado 
 *          400:
 *              description: El id del metodo de pago no existe, por favor verifique su solicitud
 *      
 */
router.put('/selectPayMeth/:id', middlewareU.emailToken, Order.PayMeth);

/**
 * @swagger
 * /ordenes/selectAddress/{id}:
 *  put:     
 *      parameters:
 *          -  in: path
 *             name: id
 *             description: id de tu direccion para la orden
 *             required: true
 *             type: integer
 * 
 *      summary: accion para seleccionar la direccion para la orden actual
 *      tags: [Ordenes]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/postOrders'
 *      responses:
 *          200:
 *              description: Se ha seleccionado una direccion para la orden
 *          400:
 *              description: El id de la direccion no existe en el directorio, por favor verifique su solicitud
 *      
 */
router.put('/selectAddress/:id', middlewareU.emailToken, Order.selectAdress);

/**
 * @swagger
 * /ordenes/confirmOrder/{id}:
 *  put:     
 *      parameters:
 *          -  in: path
 *             name: id
 *             description: id de la orden a confirmar
 *             required: true
 *             type: integer
 * 
 *      summary: El usuario  confirma la orden actual
 *      tags: [Ordenes]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/postOrders'
 *      responses:
 *          200:
 *              description: Su orden ha sido confirmada
 *          400:
 *              description: El id de la orden pendiente no existe, por favor verifique su solicitud
 *      
 */
router.put('/confirmOrder/:id', middlewareU.emailToken ,Order.confirmOrder);

/**
 * @swagger
 * /ordenes/changeStateOrder/{id}:
 *  put:     
 *      parameters:
 *          -  in: path
 *             name: id
 *             description: id de la orden para cambiar su estado por en preparacion, enviada o entregada
 *             required: true
 *             type: integer
 * 
 *      summary: El admin podrá cambiar el estado de las ordenes confirmadas
 *      tags: [Ordenes]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/ordersAdmin2'
 *      responses:
 *          200:
 *              description: El estado de la orden a sido actualizado
 *          400:
 *              description: El id de la orden no xiste, por favor verifique su solicitud
 *      
 */
router.put('/changeStateOrder/:id', middlewareU.AdminToken, middlewareU.emailToken, Order.changeStateOrder);

//Schemas Swagger
/**
 * @swagger 
 * tags: 
 *  name : 'Ordenes'
 *  description: 'CRUD para ordenes en Delilah Resto'
 * 
 * components: 
 *  schemas:
 *      ordersAdmin: 
 *          type: object
 *          required:
 *               -email
 *          properties:
 *              email:
 *                  type: string
 *                  example: admin@gmail.com
 *                  description: email del ADMIN para gestionar ordenes
 * 
 *                  $ref: '#/components/schemas/ordersAdmin'                 
 */

/**
 * @swagger 
 * tags: 
 *  name : 'Ordenes'
 *  description: 'CRUD para ordenes en Delilah Resto'
 * 
 * components: 
 *  schemas:
 *      ordersAdmin2: 
 *          type: object
 *          required:
 *               -email
 *               -stateOrder
 *          properties:
 *              email:
 *                  type: string
 *                  example: admin@gmail.com
 *                  description: email del ADMIN para gestionar ordenes
 *              stateOrder: 
 *                  type: string
 *                  example: preparacion
 *                  description: estado de orden
 * 
 *                  $ref: '#/components/schemas/ordersAdmin2'                 
 */

/**
 * @swagger 
 * tags: 
 *  name : 'Ordenes'
 *  description: 'CRUD para ordenes en Delilah Resto'
 * 
 * components: 
 *  schemas:
 *      postOrders: 
 *          type: object
 *          required:
 *               -email
 *          properties:
 *              email:
 *                  type: string
 *                  example: jorge123@gmail.com
 *                  description: email del usuario para gestionar ordenes
 * 
 *                  $ref: '#/components/schemas/postOrders'                 
 */

module.exports = router;