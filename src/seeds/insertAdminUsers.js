const bcrypt = require("bcrypt");
const db = require("../database/db");
const userValidation = require("../schemas/usuarioSchema");
//admin y usuario
const adminPass = process.env.DB_PASSWORD;

async function Admin(name, password, email, phone, isAdmin) {
  name, password, email, phone, (isAdmin = userValidation.validateAsync());
  await db.Users.findOrCreate({
    where: {
      name: "AdminDelilah",
      isAdmin: true
    },
    defaults: {  
      name: "AdminDelilah",
      password: bcrypt.hashSync(adminPass, 10),
      email: "admin@gmail.com",
      phone: 3113518004,
      isAdmin: true,
    }
  });
}

async function User(name, password, email, phone, isAdmin) {
  name, password, email, phone, (isAdmin = userValidation.validateAsync());
  await db.Users.findOrCreate({
    where: {
      name: "Jorge Arturo",
      isAdmin: false
      },
      defaults: {
        name: "Jorge Arturo",
        password: bcrypt.hashSync("jorge", 10),
        email: "jorge123@gmail.com",
        phone: 3147087873,
        isAdmin: false,
      }
  });
}

module.exports = Admin(), User();
