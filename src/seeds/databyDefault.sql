USE sprint3;

-- mysql -u root -p sprint3 <C:\Users\Pavilion\Desktop\Programación\ACAMICA\Sprint1\SprintProjectTemplate\src\seeds\databyDefault.sql
-- Enter password: ***

-- Products

INSERT INTO products ( productName, price )
VALUES ( 'Hamburguesa clasica', 350 );

INSERT INTO products ( productName, price )
VALUES ( 'Sandwich Veggie', 310 );

INSERT INTO products ( productName, price )
VALUES ( 'Ensalada Veggie', 320 );

INSERT INTO products ( productName, price )
VALUES ( 'Bagel de Salmon', 425 );

INSERT INTO products ( productName, price )
VALUES ( 'Papas a la francesa', 240 );

INSERT INTO products ( productName, price )
VALUES ( 'Focaccia', 300 );

INSERT INTO products ( productName, price )
VALUES ( 'Limonada', 50 );

INSERT INTO products ( productName, price )
VALUES ( 'Copa de vino', 80 );

-- Pay methods

INSERT INTO payMethods ( payMethodName )
VALUES ( 'Cupon de descuento' );

INSERT INTO payMethods ( payMethodName )
VALUES ( 'Efectivo' );

INSERT INTO payMethods ( payMethodName )
VALUES ( 'Tarjeta debido' );

INSERT INTO payMethods ( payMethodName )
VALUES ( 'Tarjeta credito' );

INSERT INTO payMethods ( payMethodName )
VALUES ( 'PayPal' );

-- direcciones

INSERT INTO addresses ( place, userId)
VALUES ( 'la aldea Mz D #2', 1);

INSERT INTO addresses ( place, userId)
VALUES ( 'Villa Andre Mz d casa 3', 1);

INSERT INTO addresses ( place, userId )
VALUES ( 'Hollywood #34 - 45', 2);

INSERT INTO addresses ( place, userId )
VALUES ( 'La pavona Armenia casa 50 calle 21', 2);