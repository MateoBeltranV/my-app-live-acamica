const redis = require('redis');

const clientRedis = redis.createClient(6379);

exports.CacheProducts = (req, res, next) => {
    clientRedis.get('productos', (err, getProducts) => {
        if(err) throw err;

        if (getProducts) {
            res.json(JSON.parse(getProducts));
        } else {
            next();
        }
    });
}