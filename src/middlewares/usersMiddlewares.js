const db = require('../database/db');
const expressJwt = require('express-jwt');
const jwtPassword = process.env.SERVER_PASSWORD;

//middlewre de seguridad para cada endpoint, se agregan 2 excepciones
// usa el jwt para verificar si la firma es valida y despues esa informacion sacarsela del json y ponerla en una variable para pasarselo a los demas
exports.expJWT = (expressJwt({
    secret: jwtPassword,
    algorithms: ['HS256']
}));
// router.use(
//     expressJwt({
//         secret: jwtPassword,
//         algorithms: ['HS256'],
//     }).unless({
//         path: ['/usuarios/login', '/registrar'],
//     })
// );

exports.invalidToken = ( async  (err, req, res, _next) => {
    if (err.name ==='UnauthorizedError'){
        await res.status(401).json('Token invalido');
    } else {
        await res.status(500).json('Internal server error');
    }
});

exports.AdminToken = (async (req, res, next) =>{
    const { email } = req.user;      
    const AdminAccess = await db.Users.findOne({
        where: {
            isAdmin: true
        }
    });  
        if( AdminAccess.email != email ){
            res.status(403).json('Usted no cuenta con credenciales de Administrador de esta app');
        }else{
            next();
        }
});

exports.NoRepeatUsers = (async (req, res, next) =>{
    const { email } = req.body;
    const NoRepeatEmail = await db.Users.findOne({
        where: {
            email
        }
    });
    if( NoRepeatEmail ){
        res.status(400).json('El Email suministrado ya esta registrado, porfavor ingrese otro');
    }else{
        next();
    }
});

exports.emailToken = (async (req, res, next) => {
    const getEmailToken = req.user.email;
    const { email } = req.body;
     //se pide el email por el destructuring enviado por el body, luego, se busca el User en la BD
    const userToken = await db.Users.findOne({
        where: {
            email
        }
    });
    if (userToken){
        if(getEmailToken == userToken.email){
          next();
        } else {
            res.status(401).json('El token no coincide con el Email registrado');
        }
    } else {
        res.status(400).json('El usuario no se encuentra registrado, vuelva a digitar su Email')
    }
});

//Verifica que el si el isActive esta activo continue con el endpoint, si no, esta inhabilidado
exports.uStateActive = (async (req, res, next) => {
    const token = req.user.email
    const StateUser = await db.Users.findOne({
        where: {
            isActive: false,
            email: token
        }
    });

    if(!StateUser){
        next()
    }else{
        res.status(400).json('Usted por el momento esta inhabilitado');
    }
});

