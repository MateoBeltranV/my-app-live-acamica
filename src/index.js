const express = require('express');
const helmet = require('helmet');
const db = require('./database/db');
const app = express();
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const swaggerOptions = require('./utils/swaggerOptions');

require('dotenv').config('../.env');
const PORT = process.env.PORT || 3000;

app.use(express.json());
app.use(helmet());
// app.use(express.urlencoded({extended: false}));

const usuarioRoutes = require('./routes/usuario.route');
const productoRoutes = require('./routes/productos.routes');
const direccionRoutes = require('./routes/direcciones.routes');
const medioPagoRoutes = require('./routes/mediosDePago.routes');
const ordenRoutes = require('./routes/ordenes.routes');

const { application } = require('express');
const swaggerSpecs = swaggerJsDoc(swaggerOptions);

app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerSpecs));
//en el require va usuarioRoutes, asi mismo se cita en el app.use como conección a la ruta
app.use('/usuarios', usuarioRoutes);
app.use('/productos', productoRoutes); 
app.use('/direcciones', direccionRoutes);
app.use('/medioDePago', medioPagoRoutes);
app.use('/ordenes', ordenRoutes);


//en el req.user va la informacion que contiene el token
app.get('/test', (req, res) => {
    res.json(req.user);
});

app.listen(PORT, function (){
    console.log(`La app ha arrancado en http://localhost:${PORT}`);

    //conenctantdo a la bd
    //force False no borra la base db cuando se ejecuta la app.js
    db.sequelize.sync({ force: false }).then(() =>{
        console.log("Nos hemos conectado a la base de datos");
        
        async function defaultUserAdmin() {
           await require('./seeds/insertAdminUsers');
        }
         defaultUserAdmin();  
         
    }).catch(error =>{
        console.log('Se ha producido un error', error);
    })
});

module.exports = app;